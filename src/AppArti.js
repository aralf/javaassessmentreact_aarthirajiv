import logo from './logo.svg';
import './App.css';
import Greet from "./components/Greet";
import AddPayment from "./components/AddPayment";
import PaymentTable from "./components/PaymentTable";

function App() {
  return (

    <div className="container">
      <header className="App-header">

          <img src="allstate_logo.jpg" className="App-logo" alt="mylogo" />
      </header>
        <h1 class="text-center">Arti's Payment SpringBoot-React App</h1>

        <PaymentTable></PaymentTable>
        <AddPayment></AddPayment>
    </div>
  );
}

export default App;
