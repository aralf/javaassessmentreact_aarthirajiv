import React, {Component} from "react";
import axios from "axios";
import {
    BrowserRouter as Router,
    Switch,
    Link,
    Route,
    useRouteMatch,
    useParams
} from 'react-router-dom'
import AddPayment from "./AddPayment";
import EditPayment from "./EditPayment";


export default  class PaymentTable extends Component {

    constructor(props) {
        super(props);
        //this.addPayment = this.addPayment.bind(this);
        this.state = {
            paymentList:[], //of type array
            count:0 // of type integer
            //id:0
        }
    }
    // http://localhost:8080/api/payme/getAll - below is how to access this URL from code
    componentDidMount() {
        axios
            .get("http://localhost:8080/api/payme/getAll")
            .then(response => {
                // create an array of contacts only with relevant data
                console.log(response);
                const newPayments = response.data.map(c => {
                    return {
                        id: c.id,
                        type: c.type,
                        amount: c.amount,
                        custId: c.custId,
                        date: c.paymentDate
                    };
                });
                this.setState({paymentList: newPayments})

                // debug printout
                console.log(newPayments);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render(){

        return (
            <Router>
            <div>
                <h1>Payments</h1>
                <h4 class="text-right"><Link to="/add">Add</Link></h4>

                <table class="table table-striped table-hover">
                    <tr><th>Id</th><th>Type</th><th>Amount</th><th>Customer Id</th><th>Date</th></tr>
                    <tbody>
                    {this.state.paymentList.map(d => (<tr>
                                                        <td>
                                                            cc
                                                        </td>
                                                        <td> {d.type} </td>
                                                        <td> {d.amount} </td>
                                                        <td> {d.custId} </td>
                                                        <td> {d.date} </td>
                                                      </tr>))}
                    </tbody>
                </table>

                <Switch>
                    <Route path="/add">
                        <AddPayment></AddPayment>
                    </Route>
                    <Route path="/edit">
                        <EditPayment></EditPayment>
                    </Route>
                </Switch>

            </div>
            </Router>

        )

    }

}