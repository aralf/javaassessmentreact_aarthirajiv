import React, {Component} from "react";
import axios from "axios";
import Moment from "moment";
import {
    BrowserRouter as Router,
    Switch,
    Redirect,
    Link,
    Route,
    useRouteMatch,
    useParams
} from 'react-router-dom'
import AddPayment from "./AddPayment";
import EditPayment from "./EditPayment";
import {useState} from "react/cjs/react.production.min";


export default  class PaymentTable extends Component {

    constructor(props) {
        super(props);
        //this.addPayment = this.addPayment.bind(this);
        this.state = {
            paymentList:[], //of type array
            count:0 // of type integer
            //id:0
        }
    }
    // http://localhost:8080/api/pay/getAll - below is how to access this URL from code
    componentDidMount() {
        axios
            .get("http://localhost:8080/api/pay/getAll")
            .then(response => {
                // create an array of contacts only with relevant data
                console.log(response);
                const newPayments = response.data.map(c => {
                    return {
                        id: c.id,
                        type: c.type,
                        amount: c.amount,
                        custId: c.custId,
                        date: Moment(c.paymentDate).format('DD-MMM-YYYY')
                    };
                });
                this.setState({paymentList: newPayments})

                // debug printout
                console.log(newPayments);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render(){
        return (
            <Router>
            <div>
                <h2>Payments</h2>
                <h4 class="text-right"><Link className="btn btn-info" to="/add">Add</Link></h4>


                <table class="table table-striped table-hover">
                    <tr><th>Id</th><th>Amount</th><th>Type</th><th>Customer Id</th><th>Date</th></tr>
                    <tbody>
                    {this.state.paymentList.map(d => (<tr>
                                                        <td>
                                                            <Link to={`/edit/${d.id}`}>{d.id}</Link>
                                                        </td>
                                                        <td> {d.amount} </td>
                                                        <td> {d.type} </td>
                                                        <td> {d.custId} </td>
                                                        <td> {d.date} </td>
                                                      </tr>))}
                    </tbody>
                </table>

                <Switch>
                    <Route path="/add">
                        <AddPayment></AddPayment>
                    </Route>
                    <Route path={`/edit/:paymentId`}>
                        <EditPayment></EditPayment>
                    </Route>

                </Switch>

            </div>
            </Router>

        )

    }
}
