import React, {Component} from 'react';
import axios from "axios";
import {withRouter} from "react-router";
import {
    BrowserRouter as Router,
    Switch,
    Link,
    Route,
    useRouteMatch,
    useParams
} from 'react-router-dom'
import Moment from "moment";

class EditPayment extends Component {

    constructor(props) {
        super(props);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeCustId = this.onChangeCustId.bind(this);
        this.updatePayment = this.updatePayment.bind(this);
        this.deletePayment = this.deletePayment.bind(this);

        this.state = {
            id: 0,
            amount: 0,
            type: "",
            custId: 0,
            submitted: false
        };

        //this.state.paymentId = this.props.paymentId;
    }


    // http://localhost:8080/api/pay/findById - below is how to access this URL from code
    componentDidMount() {
        axios
            .get(`http://localhost:8080/api/pay/findById/${this.props.match.params.paymentId}`)
            .then(response => {
                // create an array of contacts only with relevant data
                console.log(response);
                const newPayment = response.data;
                this.setState( {
                    id: newPayment.id,
                    amount: newPayment.amount,
                    type: newPayment.type,
                    custId: newPayment.custId,
                    submitted: false
                });

                // debug printout
                console.log(newPayment);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }
    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }
    onChangeCustId(e) {
        this.setState({
            custId: e.target.value
        });
    }

    updatePayment() {
        var data = {
            id: this.state.id,
            amount: this.state.amount,
            type: this.state.type,
            custId: this.state.custId
        }

        axios.post('http://localhost:8080/api/pay/update', data)
            .then(response => this.setState({ submitted: response.data }))
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    deletePayment() {
        console.log(this.state);
        axios.post(`http://localhost:8080/api/pay/delete/${this.state.id}`)
            //.then(response => this.setState({ submitted: response.data }))
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        console.log(this.props);
        console.log(this.state);
        return (<form>
            <h2>Edit Payment</h2>
            <div className="form-group">
                <label htmlFor="txtname"> Id </label>
                <input type="text" className="form-control" id="txtname" aria-describedby="Id"
                       placeholder="Id" value={this.state.id} />
            </div>

            <div className="form-group">
                <label htmlFor="txtname"> Amount </label>
                <input type="text" className="form-control" id="txtname" aria-describedby="amount"
                       placeholder="Enter amount" value={this.state.amount} onChange={this.onChangeAmount}/>
            </div>

            <div className="form-group">
                <label htmlFor="txtline1"> Type </label>
                <input type="text" className="form-control" id="txtline1" aria-describedby="typw"
                       placeholder="Enter type" value={this.state.type} onChange={this.onChangeType}/>
            </div>

            <div className="form-group">
                <label htmlFor="exampleid"> Customer Id </label>
                <input type="number" className="form-control" id="exampleid" aria-describedby="related Customer Id"
                       placeholder="Enter Customer Id" value={this.state.custId} onChange={this.onChangeCustId}/>
            </div>
            <div className="form-group">

                <button class="btn btn-info btn-outline-info" onClick={this.updatePayment}>Save</button>
                <button class="btn btn-danger btn-outline-danger" onClick={this.deletePayment}>Delete</button>
            </div>

        </form>)
    }
}

export default withRouter(EditPayment);
