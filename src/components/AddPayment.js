import React, {Component} from 'react';
import axios from "axios";


export default class AddPayment extends Component {

    constructor(props) {
        super(props);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeCustId = this.onChangeCustId.bind(this);
        this.savePayment = this.savePayment.bind(this);

        this.state = {
            amount: 0,
            type: "",
            custId: 0,
            submitted: false
        };
    }

    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }
    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }
    onChangeCustId(e) {
        this.setState({
            custId: e.target.value
        });
    }

    savePayment() {
        var data = {
            amount: this.state.amount,
            type: this.state.type,
            custId: this.state.custId
        }

        axios.post('http://localhost:8080/api/pay/save', data)
            .then(response => this.setState({ submitted: response.data }))
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        return (<form>
            <h2>Add Payment</h2>
            <div className="form-group">
                <label htmlFor="txtname"> Amount </label>
                <input type="text" className="form-control" id="txtname" aria-describedby="amount"
                       placeholder="Enter amount" value={this.state.amount} onChange={this.onChangeAmount}/>
            </div>

            <div className="form-group">
                <label htmlFor="txtline1"> Type </label>
                <input type="text" className="form-control" id="txtline1" aria-describedby="typw"
                       placeholder="Enter type" value={this.state.type} onChange={this.onChangeType}/>
            </div>

            <div className="form-group">
                <label htmlFor="exampleid"> Customer Id </label>
                <input type="number" className="form-control" id="exampleid" aria-describedby="related Customer Id"
                       placeholder="Enter Customer Id" value={this.state.custId} onChange={this.onChangeCustId}/>
            </div>
            <div className="form-group">

                <button class="btn btn-info"  onClick={this.savePayment}>Save</button>
            </div>

        </form>)
    }
}

