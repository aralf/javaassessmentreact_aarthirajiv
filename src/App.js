import logo from './logo.svg';
import './App.css';
import PaymentTable from "./components/PaymentTable";

function App() {
    return (

        <div className="container">
            <header className="App-header">
                <img src="allstate_logo.jpg" className="App-logo" alt="mylogo" />
                <h3 className="text-center">Arti's Payment App</h3>
            </header>

            <PaymentTable></PaymentTable>
        </div>
    );
}

export default App;

