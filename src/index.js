import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(
 <React.StrictMode>
  <App />
  </React.StrictMode>,
  document.getElementById('root')
);
//
// const header=React.createElement("H1", null, "Bold React project");
// const par=React.createElement("p", {id:"p1"}, "Icecream store says Hello!");
// const ul=React.createElement("ul", {class:"icreamlist"}, [
//                             React.createElement("li", null, "White Chocho"),
//                             React.createElement("li", null, "Vanilla"),
// ]);
// const main=React.createElement("div", null, [header, par, ul]);
//
// ReactDOM.render(main, document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
