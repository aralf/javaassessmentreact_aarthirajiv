import React from 'react';
import logo from './logo.svg';
import './App.css';
import $ from 'jquery';
import "bootstrap/dist/css/bootstrap.min.css";
import {Switch, Route, Link, BrowserRouter} from "react-router-dom";

import Welcome from "./components/Welcome"

import { Navbar, NavItem, NavDropdown, MenuItem, Nav } from 'react-bootstrap';
import Main from "./pages/Main";
import AddPayment from "./components/AddPayment";
import UpdatePayment from "./components/UpdatePayment";
import Manager from "./pages/Manager";
import About from "./components/About";
import PaymentsCardList from "./components/PaymentsCard";
import ListPosts from "./components/Posts";


function App() {

    return (

        <BrowserRouter>
            <div id="container">

                <Navbar bg="light" expand="lg">
                    <Navbar.Brand href="#home">Demo UI</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                            <Nav.Link href="/about">Home</Nav.Link>


                            <NavDropdown title="Payroll" id="basic-nav-dropdown">
                                <NavDropdown.Item  href="/main">Main</NavDropdown.Item>
                                <NavDropdown.Item href="/addpayments">Add Payments</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item href="/cards">Payment Cards</NavDropdown.Item>
                                <NavDropdown.Item href="/posts">Posts</NavDropdown.Item>
                            </NavDropdown>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>

                <div className="container mt-3">
                    <Switch>
                        <Route exact path={["/", "/home"]} component={Welcome} />
                        <Route exact path="/main" component={Main} />
                        <Route exact path="/manager/main" component={Main} />
                        <Route  path="/manager/:id" component={Manager} />
                        <Route exact path="/addpayments" component={AddPayment} />
                        <Route exact path="/about" component={About} />
                        <Route exact path="/posts" component={ListPosts} />
                        <Route  path="/payments/:id" component={UpdatePayment} />
                        <Route  exact path="/cards" component={PaymentsCardList} />
                    </Switch>
                </div>

            </div>
        </BrowserRouter>
    );
}

export default App;
